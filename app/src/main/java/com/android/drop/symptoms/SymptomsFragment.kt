package com.android.drop.symptoms

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.drop.R
import com.android.drop.databinding.FragmentSymptomsBinding
import com.android.drop.models.Symptom
import com.android.drop.symptoms.adapter.SymptomsAdapter
import com.hsalf.smilerating.SmileRating
import kotlinx.coroutines.*

val symptomData = listOf(
    Symptom("Headache", SmileRating.OKAY),
    Symptom("Stomachache", SmileRating.OKAY),
    Symptom("Cramps", SmileRating.OKAY),
    Symptom("Bloating", SmileRating.OKAY),
    Symptom("Flow", SmileRating.OKAY),
    Symptom("Cravings", SmileRating.OKAY),
    Symptom("Sleep", SmileRating.OKAY)
)

class SymptomsFragment : Fragment(R.layout.fragment_symptoms) {

    private var _binding: FragmentSymptomsBinding? = null
    private val binding get() = _binding!!

    private var viewScope = CoroutineScope(Dispatchers.Main.immediate + SupervisorJob())

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentSymptomsBinding.bind(view)
        setupRecyclerView()
    }

    private fun setupRecyclerView() = viewScope.launch(Dispatchers.Default) {
        try {
            binding.apply {
                recyclerViewSymptoms.layoutManager =
                    LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

                recyclerViewSymptoms.adapter = SymptomsAdapter(requireContext())
                recyclerViewSymptoms.setHasFixedSize(true)
                recyclerViewSymptoms.itemAnimator = null
            }

            (binding.recyclerViewSymptoms.adapter as SymptomsAdapter).adapterData =
                symptomData.shuffled()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewScope.cancel()
        _binding = null
    }

}
