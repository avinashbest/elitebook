package com.android.drop.symptoms.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.drop.R
import com.android.drop.models.Symptom
import com.google.android.material.textview.MaterialTextView
import com.hsalf.smilerating.BaseRating.Smiley
import com.hsalf.smilerating.SmileRating

class SymptomsAdapter(private val mContext: Context) :
    RecyclerView.Adapter<SymptomsAdapter.SymptomsViewHolder>() {

    var adapterData = listOf<Symptom>()

    inner class SymptomsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titleText: MaterialTextView = view.findViewById(R.id.title_text_view)
        val smileRating: SmileRating = view.findViewById(R.id.smile_rating)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SymptomsViewHolder {
        val itemLayout =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_symptoms_recycler_view, parent, false)
        return SymptomsViewHolder(itemLayout)
    }

    override fun onBindViewHolder(holder: SymptomsViewHolder, position: Int) {
        with(holder) {
            titleText.text = adapterData[position].title
            smileRating.selectedSmile = adapterData[position].smileRating
        }
    }

    override fun getItemCount() = adapterData.size

    override fun getItemId(position: Int): Long {
        return adapterData[position].hashCode().toLong()
    }

}