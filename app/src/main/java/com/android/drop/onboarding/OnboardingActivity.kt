package com.android.drop.onboarding

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.android.drop.R
import com.android.drop.auth.LoginActivity
import com.android.drop.databinding.ActivityOnboardingBinding
import com.android.drop.utils.AppPrefs

class OnboardingActivity : AppCompatActivity(), ChangePageOfViewPagerListener.ChangePage {

    private lateinit var binding: ActivityOnboardingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnboardingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (AppPrefs.getCurrentUserOnboarded(this)) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

        ChangePageOfViewPagerListener.setChangePageOfViewPager(this)
        setupViewPager()
    }

    private fun setupViewPager() {
        try {
            val adapter = OnboardingPagerAdapter(supportFragmentManager)
            binding.viewPager.adapter = adapter

            val dots = arrayOf(binding.previousDateFragmentDot, binding.cycleLengthFragmentDot)
            binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int,
                ) {

                }

                override fun onPageSelected(position: Int) {
                    for (i in 0..1) {
                        dots[i].setImageResource(R.drawable.dot_inactive)
                    }
                    dots[position].setImageResource(R.drawable.dot_active)
                }

                override fun onPageScrollStateChanged(state: Int) {

                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun setCurrentItem(pagePosition: Int) {
        binding.viewPager.setCurrentItem(pagePosition, true)
    }
}

private const val TAG = "OnboardingActivity"
