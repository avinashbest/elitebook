package com.android.drop.onboarding

object ChangePageOfViewPagerListener {

    lateinit var changePage: ChangePage

    @JvmStatic
    fun setChangePageOfViewPager(changePage: ChangePage) {
        this.changePage = changePage
    }

    interface ChangePage {
        fun setCurrentItem(pagePosition: Int)
    }
}