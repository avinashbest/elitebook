package com.android.drop.onboarding

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.core.view.isVisible
import com.android.drop.R
import com.android.drop.databinding.FragmentOnboardingPreviousDateBinding
import com.android.drop.utils.AppPrefs
import kotlinx.coroutines.*
import java.util.*

class OnboardingPreviousDateFragment : Fragment(R.layout.fragment_onboarding_previous_date) {

    private var _binding: FragmentOnboardingPreviousDateBinding? = null
    private val binding get() = _binding!!

    private var viewScope = CoroutineScope(Dispatchers.Main.immediate + SupervisorJob())

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentOnboardingPreviousDateBinding.bind(view)

        Handler(Looper.getMainLooper()).postDelayed({
            binding.apply {
                onboardingAnimation.pauseAnimation()
                onboardingAnimation.isVisible = false
                textViewAnimation.isVisible = false
            }

            binding.onboardingLayout.isVisible = true
            setupOnboardingScreen()
        }, 1000)
    }

    private fun setupOnboardingScreen() {
        binding.apply {
            // Set date picker to current date
            val calendar = Calendar.getInstance()
            datePicker.init(
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH),
                null
            )

            fabNextBtn.setOnClickListener {
                ChangePageOfViewPagerListener.changePage.setCurrentItem(1)
                updatePreferences(datePicker.dayOfMonth, datePicker.month)
            }
        }
    }

    private fun updatePreferences(selectedDate: Int, selectedMonth: Int) =
        viewScope.launch(Dispatchers.Default) {
            Log.d(TAG,
                "setupOnboardingScreen: Date = $selectedDate Month = $selectedMonth")

            AppPrefs.setSelectedPreviousDate(requireContext(), selectedDate)
            AppPrefs.setSelectedPreviousMonth(requireContext(), selectedMonth)
        }

    override fun onDestroy() {
        super.onDestroy()
        viewScope.cancel()
    }
}

private const val TAG = "OnboardingPreviousDateFragment"