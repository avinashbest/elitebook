package com.android.drop.onboarding

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.android.drop.MainActivity
import com.android.drop.R
import com.android.drop.auth.LoginActivity
import com.android.drop.databinding.FragmentOnboardingCycleLengthBinding
import com.android.drop.databinding.FragmentOnboardingPreviousDateBinding
import com.android.drop.utils.AppPrefs
import kotlinx.coroutines.*

class OnboardingCycleLengthFragment : Fragment(R.layout.fragment_onboarding_cycle_length) {

    private var _binding: FragmentOnboardingCycleLengthBinding? = null
    private val binding get() = _binding!!

    private var viewScope = CoroutineScope(Dispatchers.Main.immediate + SupervisorJob())

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentOnboardingCycleLengthBinding.bind(view)

        setupOnboarding()
    }

    private fun setupOnboarding() {
        binding.apply {
            cycleLength.textColor =
                ContextCompat.getColor(requireContext(), R.color.primaryColor)
            cycleLength.minValue = 25
            cycleLength.maxValue = 90

            // Handle continue button click
            continueButton.setOnClickListener {
                updatePreferences(cycleLength.value)
                proceedToMainActivity()
            }
        }
    }

    private fun updatePreferences(cycleLength: Int) =
        viewScope.launch(Dispatchers.Default) {
            Log.d(TAG, "Cycle Length = $cycleLength")

            AppPrefs.setCurrentUserOnboarded(requireContext(), true)
            AppPrefs.setMaximumNumberOfDaysCount(requireContext(), cycleLength)
        }

    private fun proceedToMainActivity() {
        val intent = Intent(activity, LoginActivity::class.java)
        startActivity(intent)
        activity?.finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewScope.cancel()
        _binding = null
    }
}

private const val TAG = "OnboardingCycleLengthFragment"