package com.android.drop.onboarding

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class OnboardingPagerAdapter(fragments: FragmentManager) : FragmentPagerAdapter(fragments) {

    private val list = arrayListOf<Fragment>()

    init {
        list.add(OnboardingPreviousDateFragment())
        list.add(OnboardingCycleLengthFragment())
    }

    override fun getCount() = list.size

    override fun getItem(position: Int) = list[position]
}