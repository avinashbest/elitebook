package com.android.drop.models

data class Symptom(
    val title: String,
    val smileRating: Int
)