package com.android.drop.models

data class Blog(
    val titleText: String,
    val thumbnailImageUrl: String,
    val readMoreUrl: String
)
