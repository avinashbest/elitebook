package com.android.drop.blogs

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.drop.R
import com.android.drop.blogs.adapter.BlogsAdapter
import com.android.drop.databinding.FragmentBlogsBinding
import com.android.drop.models.Blog
import kotlinx.coroutines.*


val blogData = listOf(
    Blog(
        "The Importance of Menstrual Hygiene Management for Sexual and Reproductive Health",
        "https://miro.medium.com/v2/resize:fit:1400/format:webp/1*JCxD32xvXQtnLlwCwLhY4A.png",
        "https://medium.com/@wilsonlucyc/the-importance-of-menstrual-hygiene-management-for-sexual-and-reproductive-health-313d1fc4c45"
    ),
    Blog(
        "Goodbye to Period Cramps?",
        "https://miro.medium.com/v2/resize:fit:720/format:webp/1*WbyJJXLNHvRTpgKXBc27Jg.jpeg",
        "https://divyabhat10.medium.com/goodbye-to-period-cramps-9e10d2093acc"
    ),
    Blog(
        "No Wonder You Have Pain",
        "https://miro.medium.com/v2/resize:fit:720/format:webp/1*zD_VJqcT95-baLdx4PEMvA.jpeg",
        "https://medium.com/move-me-poetry/no-wonder-you-have-pain-d155d10b7512"
    ),
    Blog(
        "Mood Swings During Your Period",
        "https://miro.medium.com/v2/resize:fit:720/format:webp/1*gs6t0S-SFownpuDxNbyLFA.jpeg",
        "https://medium.com/periodmovement/mood-swings-during-the-period-d7df953cec19"
    ),
    Blog(
        "Period",
        "https://miro.medium.com/v2/resize:fit:720/format:webp/1*-KVadUJjbrtg38f71oNLGg@2x.jpeg",
        "https://medium.com/@mjgorringe/period-25c8df4f9fc"
    ),
    Blog(
        "Managing Heavy Periods: 20 Tips and Tricks for Coping with Heavy Flow",
        "https://miro.medium.com/v2/resize:fit:720/0*zkhmNftN0lqnqjF2",
        "https://pandeez.medium.com/managing-heavy-periods-20-tips-and-tricks-for-coping-with-heavy-flow-174904e62217"
    ),
    Blog(
        "Period Playlist.",
        "https://miro.medium.com/v2/resize:fit:720/format:webp/1*zD_VJqcT95-baLdx4PEMvA.jpeg",
        "https://medium.com/3-minutes/period-playlist-91631771607"
    ),
    Blog(
        "Dysmenorrhea is a serious affair",
        "https://miro.medium.com/v2/resize:fit:720/format:webp/1*7emvNKDD98Rak8VbgWCFfw.jpeg",
        "https://anupthar.medium.com/dysmenorrhea-is-a-serious-affair-dedce437566d"
    ),
    Blog(
        "WASH-M (Water, sanitation, hygiene and menstrual health)",
        "https://miro.medium.com/v2/resize:fit:720/format:webp/1*UpAf8YToWylnJiwcsWv5Rg.jpeg",
        "https://splash.medium.com/wash-m-water-sanitation-hygiene-and-menstrual-health-8498453b133"
    ),
    Blog(
        "Making Menstrual Health Management an Everyday Priority, from Coffee Tables to Policy Tables",
        "https://miro.medium.com/v2/resize:fit:720/format:webp/1*SsnIU-yqE-RgeEhz-2JBwg.png",
        "https://womendeliver.medium.com/making-menstrual-health-management-an-everyday-priority-from-coffee-tables-to-policy-tables-c27fbbdc1d17"
    ),
    Blog(
        "Cut the Cramps",
        "https://miro.medium.com/v2/resize:fit:720/format:webp/1*5R0Dey6zlMVoaLnfEOMs2A.png",
        "https://medium.com/kaali-blog/cut-the-cramps-6b696fb90f59"
    ),
    Blog(
        "6 Ways To Relieve Period Cramps At Home",
        "https://miro.medium.com/v2/resize:fit:720/0*7k8YY0GosawHYM1-",
        "https://medium.com/bouncin-and-behavin-blogs/6-ways-to-relieve-period-cramps-at-home-a7b14ddbdd2c"
    ),
    Blog(
        "Why Do Period Pains Hurt So Much?",
        "https://miro.medium.com/v2/resize:fit:720/0*TEwUteJKwhA8MCV0",
        "https://medium.com/in-fitness-and-in-health/why-do-period-pains-hurt-so-much-364b5faaa7c4"
    ),
    Blog(
        "Oh! The cramps!!",
        "https://miro.medium.com/v2/resize:fit:720/format:webp/1*buqNv8FrQ9gUbxC834tVsQ.jpeg",
        "https://medium.com/@splendidLilly/oh-the-cramps-9f4d5b9f29"
    ),
    Blog(
        "Top 5 Best Workouts for Weight Loss and Burn Stubborn Belly Fat",
        "https://miro.medium.com/v2/resize:fit:720/0*1fsjCUolfSyHeuAd",
        "https://wellnessform.medium.com/top-5-best-workouts-for-weight-loss-and-burn-stubborn-belly-fat-8b7a820882fb"
    ),
    Blog(
        "It’s time we gave our bodies the credit they deserve",
        "https://miro.medium.com/v2/resize:fit:720/format:webp/1*ayzTwt2jhasQ0fqK58b0LA.jpeg",
        "https://unpopularopinion-dw.medium.com/its-time-you-gave-your-body-the-credit-it-deserves-1c9f9eeb7d6f"
    ),
    Blog(
        "Your Extreme Bias For Motherhood Shows Your Extreme Sexism",
        "https://miro.medium.com/v2/resize:fit:720/0*z3cUUKL7geN8xrpZ",
        "https://christylrivers.medium.com/your-extreme-bias-for-motherhood-shows-your-extreme-sexism-446813371cc3"
    ),
    Blog(
        "9 Signs you may be in perimenopause.",
        "https://miro.medium.com/v2/resize:fit:720/format:webp/0*v3SPqKXB-IsNA52I.png",
        "https://medium.com/@Miyako_Hazama/9-signs-perimenopause-part1-ab7551710277"
    )
)

class BlogsFragment : Fragment(R.layout.fragment_blogs) {

    private var viewScope = CoroutineScope(Dispatchers.Main.immediate + SupervisorJob())

    private var _binding: FragmentBlogsBinding? = null
    private val binding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentBlogsBinding.bind(view)

        Handler(Looper.getMainLooper()).postDelayed({
            binding.apply {
                blogsLoadingAnimation.pauseAnimation()
                blogsLoadingAnimation.isVisible = false
                recyclerView.isVisible = true
            }
        }, 1500)

        setupRecyclerView()
    }

    private fun setupRecyclerView() = viewScope.launch(Dispatchers.Default) {
        try {
            binding.apply {
                recyclerView.layoutManager =
                    LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

                recyclerView.adapter = BlogsAdapter(requireContext())
                recyclerView.setHasFixedSize(true)
                recyclerView.itemAnimator = null
            }

            (binding.recyclerView.adapter as BlogsAdapter).adapterData = blogData.shuffled()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewScope.cancel()
        _binding = null
    }
}