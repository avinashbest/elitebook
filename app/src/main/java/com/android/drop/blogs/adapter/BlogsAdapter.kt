package com.android.drop.blogs.adapter

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat.startActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.android.drop.R
import com.android.drop.WebViewActivity
import com.android.drop.blogs.blogData
import com.android.drop.models.Blog
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.textview.MaterialTextView

class BlogsAdapter(private val mContext: Context) :
    RecyclerView.Adapter<BlogsAdapter.BlogsViewHolder>() {

    var adapterData = listOf<Blog>()

    inner class BlogsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titleText: MaterialTextView = view.findViewById(R.id.item_text_view)
        val thumbnailImage: ShapeableImageView = view.findViewById(R.id.item_image_view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlogsViewHolder {
        val itemLayout =
            LayoutInflater.from(parent.context).inflate(R.layout.item_blogs, parent, false)
        return BlogsViewHolder(itemLayout)
    }

    override fun onBindViewHolder(holder: BlogsViewHolder, position: Int) {
        with(holder) {
            itemView.setOnClickListener {
                mContext.startActivity(
                    Intent(mContext, WebViewActivity::class.java)
                        .putExtra("url", blogData[position].readMoreUrl)
                )
            }
            Glide.with(mContext).load(adapterData[position].thumbnailImageUrl)
                .placeholder(R.drawable.placeholder_image).centerCrop().into(thumbnailImage)
            titleText.text = adapterData[position].titleText
        }
    }

    override fun getItemCount() = adapterData.size

    override fun getItemId(position: Int): Long {
        return adapterData[position].hashCode().toLong()
    }
}
