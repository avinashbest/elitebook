package com.android.drop.profile

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.android.drop.R
import com.android.drop.auth.LoginActivity
import com.android.drop.databinding.FragmentProfileBinding
import com.android.drop.utils.AppPrefs
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs


class ProfileFragment : Fragment(R.layout.fragment_profile) {

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    private var viewScope = CoroutineScope(Dispatchers.Main.immediate + SupervisorJob())

    private val currentDate = Calendar.getInstance()
    private val selectedDate: Calendar = Calendar.getInstance()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentProfileBinding.bind(view)
        setupListeners()
        selectedDate.set(currentDate.weekYear,
            AppPrefs.getSelectedPreviousMonth(requireContext()),
            AppPrefs.getSelectedPreviousDate(requireContext()))
        setupProgressBar()
    }

    private fun setupListeners() = viewScope.launch {
        try {
            binding.btnSignOut.setOnClickListener {
                logoutUser()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setupProgressBar() {
        try {
            binding.apply {
                progressBarHorizontal.max =
                    AppPrefs.getMaximumNumberOfDaysCount(requireContext())
                val differenceInMillis = selectedDate.timeInMillis - currentDate.timeInMillis
                val differenceInDays = TimeUnit.MILLISECONDS.toDays(differenceInMillis).toInt()
                val currentProgress =
                    abs(differenceInDays.coerceAtMost(AppPrefs.getMaximumNumberOfDaysCount(
                        requireContext())))
                progressBarHorizontal.progress = currentProgress

                val daysLeft: Int =
                    AppPrefs.getMaximumNumberOfDaysCount(requireContext()) - currentProgress

                val text = "Your next period cycle starts after $daysLeft days."
                binding.textViewInformationCycle.text = text
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun logoutUser() = viewScope.launch {
        try {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(requireContext(), LoginActivity::class.java)
            startActivity(intent)
            activity?.finish()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewScope.cancel()
        _binding = null
    }
}
