package com.android.drop.utils

import android.content.Context
import android.preference.PreferenceManager
import java.util.Calendar

object AppPrefs {
    private const val MAXIMUM_NUMBER_OF_DAYS_COUNT = "MAXIMUM_NUMBER_OF_DAYS_COUNT"
    private const val PREVIOUS_SELECTED_DATE = "PREVIOUS_SELECTED_DATE"
    private const val PREVIOUS_SELECTED_MONTH = "PREVIOUS_SELECTED_MONTH"
    private const val IS_USER_ONBOARDED = "IS_USER_ONBOARDED"

    fun setMaximumNumberOfDaysCount(context: Context, numberOfDays: Int) {
        val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
        editor.putInt(MAXIMUM_NUMBER_OF_DAYS_COUNT, numberOfDays)
        editor.apply()
    }

    fun getMaximumNumberOfDaysCount(context: Context): Int {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getInt(MAXIMUM_NUMBER_OF_DAYS_COUNT,
            AppConstants.DEFAULT_NUMBER_OF_DAYS_IN_CYCLE)
    }

    fun setSelectedPreviousDate(context: Context, date: Int) {
        val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
        editor.putInt(PREVIOUS_SELECTED_DATE, date)
        editor.apply()
    }

    fun getSelectedPreviousDate(context: Context): Int {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getInt(PREVIOUS_SELECTED_DATE, Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
    }

    fun setSelectedPreviousMonth(context: Context, month: Int) {
        val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
        editor.putInt(PREVIOUS_SELECTED_MONTH, month)
        editor.apply()
    }

    fun getSelectedPreviousMonth(context: Context): Int {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getInt(PREVIOUS_SELECTED_MONTH, Calendar.getInstance().get(Calendar.MONTH))
    }

    fun setCurrentUserOnboarded(context: Context, isUserOnboarded: Boolean) {
        val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
        editor.putBoolean(IS_USER_ONBOARDED, isUserOnboarded)
        editor.apply()
    }

    fun getCurrentUserOnboarded(context: Context): Boolean {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getBoolean(IS_USER_ONBOARDED, false)
    }
}
