package com.android.drop

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import com.android.drop.databinding.FragmentCycleBinding
import com.android.drop.utils.AppPrefs
import com.prolificinteractive.materialcalendarview.CalendarDay
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs


class CycleFragment : Fragment(R.layout.fragment_cycle) {

    private var _binding: FragmentCycleBinding? = null
    private val binding get() = _binding!!

    private val currentDate = Calendar.getInstance()
    private val selectedDate: Calendar = Calendar.getInstance()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentCycleBinding.bind(view)
        selectedDate.set(currentDate.weekYear,
            AppPrefs.getSelectedPreviousMonth(requireContext()),
            AppPrefs.getSelectedPreviousDate(requireContext()))
        setupProgressBar()
        setupUserInterface()
    }

    private fun setupUserInterface() {
        binding.apply {
            val differenceInMillis = selectedDate.timeInMillis - currentDate.timeInMillis
            val differenceInDays = TimeUnit.MILLISECONDS.toDays(differenceInMillis).toInt()
            val currentProgress =
                abs(differenceInDays.coerceAtMost(AppPrefs.getMaximumNumberOfDaysCount(
                    requireContext())))

            val daysLeft: Int =
                AppPrefs.getMaximumNumberOfDaysCount(requireContext()) - currentProgress

            val text =
                "Today is\n${currentDate.time.toLocaleString()}\nYour period starts in $daysLeft day(s)"

            todayTextView.text = text
            calendarView.selectedDate = CalendarDay.today()
        }
    }

    private fun setupProgressBar() {
        try {
            binding.apply {
                trackerProgressBar.progressMax =
                    AppPrefs.getMaximumNumberOfDaysCount(requireContext()).toFloat()
                val differenceInMillis = selectedDate.timeInMillis - currentDate.timeInMillis
                val differenceInDays = TimeUnit.MILLISECONDS.toDays(differenceInMillis).toInt()
                val currentProgress =
                    abs(differenceInDays.coerceAtMost(AppPrefs.getMaximumNumberOfDaysCount(
                        requireContext())))
                trackerProgressBar.progress = currentProgress.toFloat()

                val daysLeft: Int =
                    AppPrefs.getMaximumNumberOfDaysCount(requireContext()) - currentProgress
                val daysElapsed: Int = currentProgress

                val text = "Days left: $daysLeft\nDays elapsed: $daysElapsed"
                binding.informationTextView.text = text
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}

private const val TAG = "CycleFragment"
