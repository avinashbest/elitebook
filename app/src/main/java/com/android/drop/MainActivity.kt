package com.android.drop

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.window.OnBackInvokedDispatcher
import androidx.fragment.app.Fragment
import com.android.drop.blogs.BlogsFragment
import com.android.drop.databinding.ActivityMainBinding
import com.android.drop.profile.ProfileFragment
import com.android.drop.symptoms.SymptomsFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var cycleFragment: CycleFragment
    private lateinit var symptomsFragment: SymptomsFragment
    private lateinit var profileFragment: ProfileFragment
    private lateinit var blogsFragment: BlogsFragment
    private val fragments: Array<Fragment>
        get() = arrayOf(cycleFragment, symptomsFragment, profileFragment, blogsFragment)

    private var selectedIndex = 0
    private val selectedFragment get() = fragments[selectedIndex]

    private fun selectFragment(selectedFragment: Fragment) {
        var transaction = supportFragmentManager.beginTransaction()
        fragments.forEachIndexed { index, fragment ->
            if (selectedFragment == fragment) {
                transaction = transaction.attach(fragment)
                selectedIndex = index
            } else {
                transaction = transaction.detach(fragment)
            }
        }
        transaction.commit()
        supportActionBar?.title = when (selectedFragment) {
            is CycleFragment -> getString(R.string.title_cycle_fragment)
            is SymptomsFragment -> getString(R.string.title_symptoms_fragment)
            is ProfileFragment -> getString(R.string.title_profile_fragment)
            is BlogsFragment -> getString(R.string.title_blogs_fragment)
            else -> ""
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (savedInstanceState == null) {
            cycleFragment = CycleFragment()
            symptomsFragment = SymptomsFragment()
            profileFragment = ProfileFragment()
            blogsFragment = BlogsFragment()

            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container_view, cycleFragment, TAG_CYCLE_FRAGMENT)
                .add(R.id.fragment_container_view, symptomsFragment, TAG_SYMPTOMS_FRAGMENT)
                .add(R.id.fragment_container_view, profileFragment, TAG_PROFILE_FRAGMENT)
                .add(R.id.fragment_container_view, blogsFragment, TAG_BLOGS_FRAGMENT)
                .commit()
        } else {
            cycleFragment =
                supportFragmentManager.findFragmentByTag(TAG_CYCLE_FRAGMENT) as CycleFragment
            symptomsFragment =
                supportFragmentManager.findFragmentByTag(TAG_SYMPTOMS_FRAGMENT) as SymptomsFragment
            profileFragment =
                supportFragmentManager.findFragmentByTag(TAG_PROFILE_FRAGMENT) as ProfileFragment
            blogsFragment =
                supportFragmentManager.findFragmentByTag(TAG_BLOGS_FRAGMENT) as BlogsFragment

            selectedIndex = savedInstanceState.getInt(KEY_SELECTED_INDEX, 0)
        }
        selectFragment(selectedFragment)

        binding.bottomNavigationHome.setOnItemSelectedListener { item ->
            val fragment = when (item.itemId) {
                R.id.nav_cycle -> cycleFragment
                R.id.nav_symptoms -> symptomsFragment
                R.id.nav_profile -> profileFragment
                R.id.nav_blogs -> blogsFragment
                else -> throw IllegalArgumentException("Unexpected itemId")
            }
            if (selectedFragment === fragment) {
//                fragment.onBottomNavigationFragmentReselected()
            } else {
                selectFragment(fragment)
            }
            true
        }
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState.putInt(KEY_SELECTED_INDEX, selectedIndex)
    }


    override fun onBackPressed() {
        if (selectedIndex != 0) {
            binding.bottomNavigationHome.selectedItemId = R.id.nav_cycle
        } else {
            super.onBackPressed()
        }
    }

}

private const val TAG_CYCLE_FRAGMENT = "TAG_CYCLE_FRAGMENT"
private const val TAG_SYMPTOMS_FRAGMENT = "TAG_SYMPTOMS_FRAGMENT"
private const val TAG_PROFILE_FRAGMENT = "TAG_PROFILE_FRAGMENT"
private const val TAG_BLOGS_FRAGMENT = "TAG_BLOGS_FRAGMENT"
private const val KEY_SELECTED_INDEX = "KEY_SELECTED_INDEX"