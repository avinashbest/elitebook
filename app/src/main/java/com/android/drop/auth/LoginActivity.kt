package com.android.drop.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.core.view.isVisible
import com.android.drop.MainActivity
import com.android.drop.databinding.ActivityLoginBinding
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import kotlinx.coroutines.*
import java.util.concurrent.TimeUnit

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private var viewScope = CoroutineScope(Dispatchers.Main.immediate + SupervisorJob())

    private var verificationId: String = ""
    private var timer: CountDownTimer? = null

    private val auth
        get() = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (auth.currentUser != null) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        setupListeners()
    }

    private fun updateWaitingInformation() {
        binding.waitingTimeInfo.isVisible = true
        timer = object : CountDownTimer(60_000L, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.waitingTimeInfo.text = "Time left ${millisUntilFinished / 1000} seconds"
            }

            override fun onFinish() {
                Snackbar.make(binding.root, "Please try again.", Snackbar.LENGTH_LONG).show()
            }
        }.start()
    }

    private fun setupListeners() = viewScope.launch {
        try {
            binding.apply {
                btnGetOtpForVerification.setOnClickListener {
                    if (TextUtils.isEmpty(enterYourMobileNumber.editText?.text.toString())) {
                        enterYourMobileNumber.error = "Please enter a valid phone number."
                        return@setOnClickListener
                    }
                    val phoneNumber = "+91" + enterYourMobileNumber.editText?.text.toString()
                    sendVerificationCode(phoneNumber)
                    updateWaitingInformation()
                }
                btnVerifyLoginOtp.setOnClickListener {
                    if (TextUtils.isEmpty(enterLoginOtp.editText?.text.toString())) {
                        enterLoginOtp.error = "Please enter the OTP."
                        return@setOnClickListener
                    }
                    verifyCode(enterLoginOtp.editText?.text.toString())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private val verificationCallback =
        object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
                super.onCodeSent(p0, p1)
                timer?.cancel()
                verificationId = p0
                binding.apply {
                    waitingTimeInfo.isVisible = false
                    btnVerifyLoginOtp.isVisible = true
                }
            }

            override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                val code = p0.smsCode
                if (code != null) {
                    binding.enterLoginOtp.editText?.setText(code)
                    verifyCode(code)
                }
            }

            override fun onVerificationFailed(p0: FirebaseException) {
                binding.waitingTimeInfo.isVisible = false
                Snackbar.make(binding.root, "${p0.message}", Snackbar.LENGTH_LONG).show()
                binding.enterLoginOtp.error = p0.message ?: "Please try again."
            }
        }

    private fun verifyCode(code: String) {
        try {
            val credential = PhoneAuthProvider.getCredential(verificationId, code)
            signInWithCredential(credential)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun signInWithCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    binding.waitingTimeInfo.isVisible = false
                    Log.d(TAG, "signInWithCredential: ${task.exception?.message}")
                    Snackbar.make(binding.root, "Please try again.", Snackbar.LENGTH_LONG).show()
                }
            }
    }

    private fun sendVerificationCode(phoneNumber: String) {
        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(phoneNumber)
            .setTimeout(60L, TimeUnit.SECONDS)
            .setActivity(this)
            .setCallbacks(verificationCallback)
            .build()

        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewScope.cancel()
    }
}

private const val TAG = "LoginActivity"
